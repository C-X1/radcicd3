"""Start minikube cluster."""

from typing import List

from radcicd3.directories import PROJECT_DIR
from radcicd3.exec import execute
from radcicd3.literals import MINIKUBE


def _minikube_ip() -> str:
    """Get minikube ip.

    :returns: minikube ip as string.
    """
    return execute([MINIKUBE, "ip"])[0]


def _minikube_bridge() -> str:
    """Return minikube network bridge id.

    :returns: minikube bridge id
    """
    route_str = execute(["ip", "route", "get", _minikube_ip()])
    return route_str[0].split(" ")[2]


def _add_minikube_resolve_route(domain: str = "ingress.local") -> None:
    """Add route by resolvectl for ingress.local.

    :param domain: Domain for local cluster
    """
    ip = _minikube_ip()
    bridge = _minikube_bridge()

    execute(["resolvectl", "dns", bridge, ip], sudo=True)
    execute(["resolvectl", "domain", bridge, domain], sudo=True)


def _additional_start_param(driver: str) -> List[str]:
    """Create the minikube cluster.

    :param driver: The driver to use to create the cluster
    :returns: List with additional start params for minikube
    """
    docker_init = [
        "--mount",
        "true",
        "--mount-string",
        f"{PROJECT_DIR}:/host",
    ]

    if driver == "docker":
        return docker_init

    return []


def _post_bringup_docker(driver: str) -> None:
    """Post bringup for docker.

    :param driver: The driver to use to create the cluster
    """
    if driver != "docker":
        return

    execute([MINIKUBE, "ssh", "--", "sudo apt-get update"])

    execute([MINIKUBE, "ssh", "--", "sudo apt-get install -y git"])


def _post_bringup_general() -> None:
    """Post bringup for all."""
    execute([MINIKUBE, "addons", "enable", "ingress"])
    execute([MINIKUBE, "addons", "enable", "ingress-dns"])
    _add_minikube_resolve_route()


def create_minikube(driver: str) -> None:
    """Create the minikube cluster.

    :param driver: The driver to use to create the cluster
    """
    status = execute(
        [
            MINIKUBE,
            "status",
        ],
        except_on_error=False,
    )

    if status[2]:  # Return if minikube exists
        execute(
            [
                MINIKUBE,
                "start",
                "--driver",
                driver,
            ]
            + _additional_start_param(driver)
        )

    _post_bringup_docker(driver)
    _post_bringup_general()
