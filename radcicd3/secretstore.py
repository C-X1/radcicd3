"""Storing and retrieving secrets."""

from typing import Union

import secretstorage
import yaml
from secretstorage.collection import Collection
from secretstorage.item import Item


class SecretStore(object):
    """Storing and extracting secrets."""

    def __init__(self) -> None:
        """Initialize class."""
        self.dbus = secretstorage.dbus_init()

    def __del__(self) -> None:  # noqa: WPS603
        """Close DBus on destruction."""
        self.dbus.close()

    def ensure_collection_exists(self) -> Collection:
        """Ensure the radcicd secret collection exists and is unlocked.

        :returns: The radcicd collection.
        """
        radcicd_collection = None
        for collection in secretstorage.get_all_collections(self.dbus):
            if collection.get_label() == "radcicd":
                radcicd_collection = collection
                break

        if radcicd_collection is None:
            radcicd_collection = secretstorage.create_collection(self.dbus, "radcicd")

        radcicd_collection.unlock()
        radcicd_collection.ensure_not_locked()

        return radcicd_collection

    def get_secret_with_label(self, label: str) -> Union[Item, None]:
        """Return first secret item with given label.

        :param label: The label to filter for.
        :returns: The item with the label or None
        """
        collection = self.ensure_collection_exists()

        for secret_item in collection.get_all_items():
            if secret_item.get_label() == label:
                return secret_item

        return None

    def insert_yaml_secret(self, label: str, secret_data: dict) -> None:
        """Insert/Update vault init data to secret store.

        :param label: The label
        :param secret_data: Secret yaml data
        """
        collection = self.ensure_collection_exists()

        collection.create_item(
            label,
            {"id": label},
            yaml.dump(secret_data),
            replace=True,
            content_type="application/x-yaml",
        )

    def get_yaml_secret(self, label: str) -> dict:
        """Get vault init secrets.

        :param label: The label of the item
        :returns: The dictionary with the secret init data
        """
        secret_item = self.get_secret_with_label(label)
        return yaml.safe_load(secret_item.get_secret().decode("UTF-8"))
