"""Implement typer example."""

import typer

from radcicd3.cli.app import app
from radcicd3.directories import create_local_dirs
from radcicd3.flux import init_flux
from radcicd3.minikube import create_minikube
from radcicd3.vault import VaultInit

DRIVER_OPTION = typer.Option(default="docker", help="The driver for minikube")


@app.command()
def start_minikube(driver: str = DRIVER_OPTION) -> None:
    """Be an example for typer.

    :param driver: The driver to use to create the minikube cluster
    """
    create_local_dirs()
    create_minikube(driver)
    init_flux()
    vault_init = VaultInit(
        "dev",
        "vault-0",
        "https://vault.ingress.local",
        on_pod=True,
        insecure=True
    )
    vault_init.init_single_user()
    vault_init.create_init_secret()
