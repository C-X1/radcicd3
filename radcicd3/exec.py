"""Execute commands on shell."""

import os
import subprocess  # noqa: S404
from typing import Dict, List, Tuple, Union

import structlog

from radcicd3.directories import TOOL_DIR
from radcicd3.sudo import sudo_passwd

logger = structlog.get_logger()


def execute(  # noqa: WPS210, WPS211
    command: List[str],
    except_on_error: bool = True,
    log: bool = True,
    env: Union[Dict[str, str], None] = None,
    sudo: bool = False,
) -> Tuple[str, str, int]:
    """Execute commands on shell.

    :param command: The command to execute
    :param except_on_error: Raise exception on non zero exit
    :param log: Logs stdout/stderr if True - use False for secret operations
    :param env: Additional environment variables
    :param sudo: This is a sudo command. Using sudo password.

    :returns: Tuple stdout, stderr, returncode
    """
    sudo_pass = None
    if sudo:
        sudo_pass = sudo_passwd()
        command = ["sudo"] + command

    if env is None:
        env = {}
    env = os.environ.copy() | env
    env["PATH"] = "{}:{}".format(env["PATH"], TOOL_DIR)

    if log:
        logger.info("Executing:\n{}\n\n".format(" ".join(command)))
    else:
        logger.info("Executing command without log.")

    proc = subprocess.Popen(
        " ".join(command),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,  # noqa: S602
        universal_newlines=True,
        env=env,
    )

    output, error = proc.communicate(
        input=sudo_pass if sudo else None
    )
    return_code = proc.returncode

    _execute_log(log, except_on_error, output, error, return_code)
    return (output, error, return_code)


def kube_execute(  # noqa: WPS211
    command: List[str],
    namespace: str,
    pod: str,
    container: str,
    except_on_error: bool = True,
    log: bool = True,
) -> Tuple[str, str, int]:
    """Execute commands on shell.

    :param namespace: Namespace of pod
    :param pod: pod name
    :param container: container to use
    :param command: The command to execute
    :param except_on_error: Raise exception on non zero exit
    :param log: Logs stdout/stderr if True - use False for secret operations

    :returns: Tuple stdout, stderr, returncode
    """
    return execute(
        [
            "kubectl",
            "exec",
            "-it",
            "-n",
            namespace,
            "-c",
            container,
            pod,
            "--",
        ]
        + command,
        log=log,
        except_on_error=except_on_error,
    )


def _execute_log(
    log: bool,
    except_on_error: bool,
    output: str,
    error: str,
    return_code: int
) -> None:
    """Log execution report.

    :param log: Logging enabled if true, redacted otherwise
    :param except_on_error: Raise exception if return code is not zero
    :param output: Stdout
    :param error: Stderr
    :param return_code: the return code

    :raises CalledProcessError: If error code not zero
    """
    if not log:
        output = "OUTPUT EXPUNGED: No log!" if output else ""
        error = "ERROR EXPUNGED: No log!" if error else ""
        command = ["COMMAND REDACTED: No log!"]

    if output:
        logger.debug(f"Stdout:\n{output}\n\n")
    if error:
        logger.debug(f"Stderr:\n{error}\n\n")
        logger.debug(f"Return Code: {return_code}\n\n\n")

    if return_code and except_on_error:
        raise subprocess.CalledProcessError(
            return_code, command, output=output, stderr=error
        )
