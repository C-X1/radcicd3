"""Contain directories for project."""

import os

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = os.path.abspath(os.path.join(SCRIPT_DIR, ".."))
FLUX_DIR = os.path.join(PROJECT_DIR, "cluster", "flux")
FLUX_APPS_DIR = os.path.join(FLUX_DIR, "apps")
LOCAL_DIR = os.path.join(PROJECT_DIR, ".local")
TOOL_DIR = os.path.join(LOCAL_DIR, "tools")
CERT_DIR = os.path.join(LOCAL_DIR, "certs")


def create_local_dirs() -> None:
    """Create local dir for downloaded tools."""
    directories = (TOOL_DIR, CERT_DIR)
    for directory in directories:
        os.makedirs(directory, exist_ok=True)
