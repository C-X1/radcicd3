"""Make radcicd3 executable by `python -m radcicd3`."""
from radcicd3 import radcicd3

if __name__ == "__main__":  # pragma: no cover
    radcicd3.main()
