"""Initialize flux repository."""

import git

from radcicd3.directories import FLUX_DIR
from radcicd3.exec import execute


def _flux_init_repo(path: str) -> git.repo.base.Repo:
    """Set all configurations for the flux repository.

    :param path: The repository path
    :returns: Repo object
    """
    repo = None
    try:
        repo = git.Repo(path)
    except git.InvalidGitRepositoryError:
        repo = git.Repo.init(path)

    return repo


def _flux_config_repository(repo: git.repo.base.Repo) -> None:
    """Set all configurations for the flux repository.

    :param repo: The repository object
    """
    config_writer = repo.config_writer()
    config_writer.set_value("user", "name", "radcicd")
    config_writer.set_value("user", "email", "flux@radcicd")
    config_writer.set_value("receive", "denyCurrentBranch", "updateInstead")
    config_writer.set_value("core", "fileMode", "false")


def _flux_repository_commit(repo: git.repo.base.Repo) -> None:
    """Commit all changes into repo.

    :param repo: The repository object
    """
    repo.index.add("*")
    repo.index.commit("...")


def _flux_repository_main(repo: git.repo.base.Repo) -> None:
    """Change into main.

    :param repo: The repository object
    """
    if repo.active_branch.name != "main":
        repo.git.branch("main")
        repo.git.checkout("main")


def init_flux() -> None:
    """Initialize Flux repository."""
    repo = _flux_init_repo(FLUX_DIR)
    _flux_config_repository(repo)
    _flux_repository_commit(repo)
    _flux_repository_main(repo)

    execute(
        [
            "flux",
            "bootstrap",
            "git",
            "--url=ssh://docker@$(minikube ip)/host/cluster/flux",
            "--private-key-file=$(minikube ssh-key)",
            "-s",
        ]
    )
