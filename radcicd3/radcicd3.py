"""Construct and run main application."""

import sys

import structlog
import typer

from radcicd3.cli.app import app
###
# Typer imports "used" by @app.command() decorator in the file
###
from radcicd3.cli.start_minikube import start_minikube  # noqa: F401


@app.callback(no_args_is_help=True)
def callback() -> None:
    """Show help when called with no arguments."""


def main() -> None:
    """Construct typer_click interface."""
    log = structlog.get_logger()

    typer_click = typer.main.get_command(app)
    try:
        typer_click()
    except Exception as exception:
        log.fatal("Caught unhandled exception")
        log.exception(exception)
        sys.exit(1)


if __name__ == "__main__":  # pragma: no cover
    main()
