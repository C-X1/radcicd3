"""Download files from pods."""

import os

from radcicd3.directories import TOOL_DIR
from radcicd3.exec import execute


def download_file_from_pod(
    namespace: str, pod: str, container: str, path: str, out_path: str
) -> None:
    """Download file from Kubernetes pod.

    :param namespace: The namespace
    :param pod: The pod name
    :param container: The container name
    :param path: The path of the file in the container
    :param out_path: The path to store the file locally
    """
    execute(
        ["kubectl", "cp", "-n", namespace, "-c", container, f"{pod}:{path}", out_path]
    )


def download_tool_from_pod(namespace: str, pod: str, container: str, path: str) -> None:
    """Download a tool from a container.

    :param namespace: The namespace
    :param pod: The pod name
    :param container: The container name
    :param path: The path of the file in the container
    """
    filename = os.path.basename(path)
    filepath = os.path.join(TOOL_DIR, filename)

    download_file_from_pod(namespace, pod, container, path, filepath)
    execute(["chmod", "+x", filepath])
