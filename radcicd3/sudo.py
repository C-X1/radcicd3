"""Set sudo password once."""

import contextvars
import getpass
import subprocess  # noqa: S404
import sys

sudo_pass_ctx = contextvars.ContextVar("sudo_pass")


def check_sudo_passwd(sudo_pass: str) -> bool:
    """Check if sudo password is correct.

    :param sudo_pass: The sudo password to test
    :returns: True if sudo password worked, false otherwise
    """
    proc = subprocess.Popen(  # noqa: S603
        " ".join([
            "echo",
            sudo_pass,
            "|",
            "sudo",
            "-S",
            "true",
            ">",
            "/dev/null",
            "2>&1"
        ]),
        shell=True,  # noqa: S602
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    out, err = proc.communicate()

    return proc.returncode == 0


def sudo_passwd() -> str:
    """Query sudo passwd from user.

    :returns: sudo password
    """
    sudo_pass = None
    try:
        sudo_pass = sudo_pass_ctx.get()

    except LookupError:
        while True:
            sys.stdout.write("Enter sudo password.\n")
            sys.stdout.flush()
            sudo_pass = getpass.getpass()

            if check_sudo_passwd(sudo_pass):
                break
            else:
                sys.stdout.write("Incorrect password, try again.\n")
                sys.stdout.flush()

        sudo_pass_ctx.set(sudo_pass)

    return sudo_pass
