"""Import vault from container."""


import base64
import os
from typing import List, Tuple

import yaml

from radcicd3.directories import FLUX_APPS_DIR
from radcicd3.download import download_tool_from_pod
from radcicd3.exec import execute, kube_execute
from radcicd3.literals import VAULT
from radcicd3.secretstore import SecretStore

VAULT_INIT_DATA_LABEL = "radcicd_vault_init"


class VaultInit(object):
    """Class for initializing Vault in the cluster."""

    def __init__(  # noqa: WPS211
        self,
        namespace: str = "dev",
        pod: str = "vault-0",
        uri: str = "https://vault.ingress.local",
        secret_label: str = VAULT_INIT_DATA_LABEL,
        on_pod: bool = False,
        insecure: bool = False,
    ) -> None:
        """Initialize class.

        :param namespace: Namespace of Vault pod
        :param pod: pod name of Vault
        :param secret_label: The label for the init secret
        :param uri: Ingress URI
        :param on_pod: Execute directly on pod
        :param insecure: Insecure execute without encryption/checks
        """
        self.namespace = namespace
        self.pod = pod
        self.secrets = SecretStore()
        self.secret_label = secret_label
        self.insecure = insecure
        self.on_pod = on_pod

    def download_vault_cli(self) -> None:
        """Download CLI tool from vault pod."""
        download_tool_from_pod(self.namespace, self.pod, "vault", "/bin/vault")

    def vault_cli(
        self, command: List[str], log: bool = True, except_on_error: bool = True
    ) -> Tuple[str, str, int]:
        """Execute something with the Vault CLI.

        :param command: List of commands for the Vault CLI
        :param except_on_error: Exception on error
        :param log: Log command, stdout and stderr disable for usage with credentials.
        :param except_on_error: Throw exception on error
        :returns: Tuple(stdout, stderr, return_code)
        """
        insec_export = ["export", "VAULT_ADDR=http://127.0.0.1:8200", "&&"]
        kube_parameters = {
            "pod": self.pod,
            "namespace": self.namespace,
            "container": "vault",
        }
        exec_parameters = {"log": log, "except_on_error": except_on_error}

        execute_fct = execute
        insec = []
        if self.on_pod:
            insec = insec_export if self.insecure else []
            execute_fct = kube_execute
            exec_parameters |= kube_parameters

        exec_parameters["command"] = [
            "sh",
            "-c",
            '"{}"'.format(" ".join(insec + [VAULT] + command)),
        ]

        return execute_fct(**exec_parameters)

    def vault_status(self) -> Tuple[dict, int]:
        """Get status from vault.

        :returns: status data, return code
        """
        yaml_str, _, return_code = self.vault_cli(
            ["status", "-format=yaml"], except_on_error=False
        )
        yaml_data = yaml.safe_load(yaml_str)

        return (yaml_data, return_code)

    def init_single_user(self) -> None:
        """Initialize the vault for single user local cluster."""
        status_data, _ = self.vault_status()

        if not status_data["initialized"]:
            self.secrets.ensure_collection_exists()

            yaml_str, _, _ = self.vault_cli(
                [
                    "operator",
                    "init",
                    "-key-threshold=1",
                    "-key-shares=1",
                    "-format=yaml",
                ],
                log=False,  # SH! Secrets!
            )
            vault_init_data = yaml.safe_load(yaml_str)

            self.secrets.insert_yaml_secret(self.secret_label, vault_init_data)

    def ensure_unsealed(self) -> None:
        """Ensure vault is unlocked."""
        status_data, _ = self.vault_status()

        if status_data["sealed"]:
            init_secrets = self.secrets.get_yaml_secret(VAULT_INIT_DATA_LABEL)
            unseal_keys = init_secrets["unseal_keys_b64"]

            for unseal_key in unseal_keys:
                status_data, _ = self.vault_status()
                if not status_data["sealed"]:
                    break

                self.vault_cli(
                    [
                        "operator",
                        "unseal",
                        unseal_key,
                    ],
                    log=False,
                )

    def create_init_secret(self) -> None:
        """Create the secret for the init job."""
        self.ensure_unsealed()

        init_secrets = self.secrets.get_yaml_secret(self.secret_label)
        root_token = init_secrets["root_token"].encode("UTF-8")
        root_token = base64.b64encode(root_token)
        root_token = root_token.decode("UTF-8")

        execute(
            [
                "cat",
                os.path.join(FLUX_APPS_DIR, "vault", "init", "secret.yaml"),
                "|",
                "sed",
                f"--expression='s/REPLACE/{root_token}/g'",
                "|",
                "kubectl",
                "apply",
                "-f",
                "-",
            ],
            log=False,
        )

    def init_cert_gen(self) -> None:
        """Init cert generation."""
        init_secrets = self.secrets.get_yaml_secret(self.secret_label)

        root_token = init_secrets["root_token"]

        vault_cli_commands = [
            f"login -token-only {root_token}",
            "secrets enable pki",
            "secrets tune -max-lease-ttl=24h pki",
            "write -field=certificate pki/root/generate/internal \
             common_name='ingress.local' \
             issuer_name='root-2022' \
             ttl=87600h -format=yaml",
        ]

        responses = []

        for vault_cli_command in vault_cli_commands:
            responses.append(
                self.vault_cli(vault_cli_command.split(" "), except_on_error=False)
            )
