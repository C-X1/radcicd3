#!/bin/sh


path_enable()
{
    basetype=$1
    file=$2

    while read line
    do
        # Remove comments
        line=$(echo "$line" | sed 's/#.*//')

        if [ -z "$line" ] || [ "$(echo "$line" | tr -d '[:space:]')" = "" ]; then
            continue
        fi

        # Split string of the line
        subtype=$(echo "$line" | awk '{print $1}')
        path=$(echo "$line" | awk '{print $2}')
        version=$(echo "$line" | awk '{print $3}')


        cmd=$(echo vault $basetype enable -path="$path")


        # Check if version defined - then add it
        if [ -n "$version" ]; then
            cmd="$cmd -version=$version"
        fi

        # Finalize command
        cmd="$cmd $subtype"

        #Execute
        $cmd

    done < "$file"
}


# Create policies
for file in "/policies"/*
do
  if [ -f "$file" ] # Check if the item is a file
  then
    policy_name=$(basename "$file" .hcl) #Get name of file as policy name
    vault policy write "$policy_name" "$file"
  fi
done

# Init engines
path_enable auth /engines/auth
path_enable secrets /engines/secrets

#Create other stuff - secrets
vault secrets tune -max-lease-ttl=24h -default-lease-ttl=24h pki/
vault write pki/root/generate/internal common_name=vault.ingress.local
