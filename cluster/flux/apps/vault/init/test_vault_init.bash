# Script for debugging job vault-init
TOKEN=$(cat ~/.vault-token) # Using token when logged in.

kubectl -n dev delete \
    job/vault-init

sleep 5

kubectl logs vault-init-



kubectl -n dev delete \
    clusterrole/vault-init-secret-delete \
    serviceaccount/vault-init-secret-delete \
    rolebinding/vault-init-secret-delete-binding \
    serviceaccount/vault-init-secret-delete \
    configmap/vault-init \
    configmap/vault-init-policies


cat secret.yaml | sed "s/REPLACE/$(printf $TOKEN | base64)/g" | kubectl -n dev apply -f -

# Test job
kubectl -n dev apply -k .

sleep 5

kubectl logs -l job-name=vault-init -n dev -c vault-init --tail=-1
