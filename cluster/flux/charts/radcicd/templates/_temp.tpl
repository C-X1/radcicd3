{{/*
 Fill templates to a maximum level
 */}}
{{- define "temp.fill.templates" }}
{{- $text := .text }}
{{- range $_, $_ := until (.max_lvl | default 20) -}}
{{- $text = tpl $text $.context }}
{{- end }}
{{- $text }}
{{- end }}

{{/*
 Replace escaped brackets in the given text.
 */}}
{{- define "temp.replace.escaped.brackets" }}
{{- $text := .text -}}
{{- $text := regexReplaceAll "\\\\{" $text "{" -}}
{{- $text := regexReplaceAll "\\\\}" $text "}" -}}
{{- $text }}
{{- end }}
