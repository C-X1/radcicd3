{{- /* Processing values */ -}}

{{- define "helm.values.expand" }}

{{- $values_file := (printf "%s/%s" ._feature.directory "values.yaml") }}
{{- $values_content_string := .Files.Get $values_file -}}

{{- /* Output */ -}}

{{ $spec := (dict) }}

{{- if (or (hasKey .feature "values")  $values_content_string) }}
{{- $values_content_string := include "temp.fill.templates" (dict "text" ($values_content_string) "context" .) }}
{{- $values_content_map :=  $values_content_string | fromYaml -}}
{{- $values_content_map := (mergeOverwrite $values_content_map (default (dict) .feature.values)) -}}

{{- $_ := set $spec "values" $values_content_map }}
{{- end }}

{{- if (hasKey .feature "valuesFrom") }}
{{- $_ := set $spec "valuesFrom" .feature.valuesFrom }}
{{- end }}

{{- $spec | toYaml -}}

{{- end }}
