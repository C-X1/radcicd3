{{/*
 Prepare the context for a single feature

 Example:
 {{ include "context.feature" (dict "feature" $feature_settings "kind" $kind "context" $context "output" $output) }}
 $feature_settings is the feature dictionary (for default only the name entry of the feature)
 $kind can be infra or feature
 $context is the normal . context
 $output A empty dictionary to take the output values
 */}}
{{- define "context.feature" -}}

{{ with .context }}
{{/* Prepare special fields */}}
{{- $_feature := dict }}
{{- $_ := set $_feature "namespace" (default .Release.Namespace $.feature.namespace) }}
{{ if (eq $.kind "infra") }}
{{- $_ := set $_feature "namespace" (default .Release.Namespace (default .Values.default.infra_namespace $.feature.namespace)) }}
{{- end }}

{{- $_ := set $_feature "release_name" (default $.feature.name $.feature.release_name) }}
{{- $_ := set $_feature "directory" (printf "features/%s" $.feature.name) }}
{{- $_ := set $_feature "feature_files" (.Files.Glob (printf "%s/**" $_feature.directory)) }}
{{- $_ := set $_feature "root_files" (.Files.Glob (printf "%s/*" $_feature.directory)) }}
{{- $_ := set $_feature "template_files" (.Files.Glob (printf "%s/*.yaml" $_feature.directory)) }}

{{/* Extend normal context with feature */}}
{{ $output := .output }}
{{- $output = merge $.output . -}}
{{- $_ := set $.output "kind" $.kind -}}
{{- $_ := set $.output "feature" $.feature }}
{{- $_ := set $.output "_feature" $_feature }}
{{- $_ := set $.output "_Values" ((tpl ($.output.Values | toYaml) $.output) | fromYaml) }}
{{- $_ := set $.output "_Values" ((include "temp.fill.templates" (dict "text" ($.output._Values | toYaml) "context" $.output)) | fromYaml) }}

{{/* Remove features array from values as probably containing bogus info */}}
{{- $_Values := $.output._Values -}}
{{- $_ := unset $_Values "features" -}}

{{- end -}}
{{- end -}}


{{/*
 Collect defaults for every feature in features/
 {{ include "context.features.defaults" (dict "feature_name" $feature "context" $context "output" $output) }}
 $context is the normal . context
 $feature is the name of the feature (also its folder name)
 $output A empty dictionary to take the output values
 */}}
{{- define "context.features.defaults" }}

{{ $_ := set $.output "defaults_feature" dict }}
{{ $_ := set $.output "defaults_infra" dict }}

{{ range $feature_name := include "file.list.feature.dirs" .context | fromJsonArray }}

{{ $defaults_feature := dict }}
{{ $defaults_infra := dict }}
{{ include "context.feature" (dict "feature" (dict "name" $feature_name) "kind" "feature" "context" $.context "output" $defaults_feature) }}
{{ include "context.feature" (dict "feature" (dict "name" $feature_name) "kind" "intra" "context" $.context "output" $defaults_infra) }}

{{ $_ := set $.output.defaults_feature $feature_name $defaults_feature}}
{{ $_ := set $.output.defaults_infra $feature_name $defaults_infra }}

{{- end -}}
{{- end -}}

{{/*
 Collect values for all normal features and infra features
 {{ $values := include "context.features.all" (dict "context" $ "output" $output) }}
 */}}
{{- define "context.features.all" }}

{{- $features := list -}}
{{- $infra_features := list -}}

{{ range $index, $feature := .context.Values.features }}
{{- $feature_context := dict }}

{{- include "context.feature" (dict "feature" $feature "kind" "feature" "context" $.context "output" $feature_context)  }}
{{ $_ := set $feature_context "index" $index }}
{{- $features = append $features $feature_context -}}
{{- end -}}

{{ range $index, $feature := .context.Values.infrastructure.features }}
{{- $feature_context := dict }}
{{- include "context.feature" (dict "feature" $feature "kind" "infra" "context" $.context "output" $feature_context)  }}
{{ $_ := set $feature_context "index" $index }}
{{- $infra_features = append $infra_features $feature_context -}}
{{- end -}}

{{ $_ := set .output "features" $features }}
{{ $_ := set .output "infra" $infra_features }}
{{- end -}}
