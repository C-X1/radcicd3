{{- /*
Process helm release files
*/ -}}
{{- define "helm.release.expand" }}

{{- $release_file := (printf "%s/%s" ._feature.directory "release.yaml") }}
{{- $release_content_string := .Files.Get $release_file -}}


{{- /* Output */ -}}

{{- if $release_content_string }}

{{- $release_content_string := include "temp.fill.templates" (dict "text" $release_content_string "context" .) }}
{{- $release_content_map :=  $release_content_string | fromYaml -}}
{{- $chart := $release_content_map.spec.chart }}

{{- /* Insert metadata */ -}}
{{- $_ := set $release_content_map "metadata" (dict) }}
{{- $metadata := $release_content_map.metadata }}
{{- $_ := set $metadata "name"  ._feature.release_name }}
{{- $_ := set $metadata "namespace"  ._feature.namespace -}}

{{- /* Overwrite spec.chart.spec */ -}}
{{- $_ := set $chart "spec" (mergeOverwrite $chart.spec (default (dict) .feature.chart_spec)) -}}

{{- /* Overwrite interval */ -}}
{{- $_ := set $chart.spec "interval" (default $chart.spec.interval (default .Values.default.reconcile_interval .feature.interval)) -}}

{{- /* Overwrite whole release */ -}}
{{- $release_content_map := (mergeOverwrite $release_content_map (default (dict) .feature.release_spec)) -}}

{{- $spec_values := $release_content_map.spec }}
{{- $spec_values := mergeOverwrite $spec_values (include "helm.values.expand" .  | fromYaml) -}}
---
## release.yaml of {{ .kind }}[{{ .index }}]
## Source: {{ $release_file }}

{{ $release_content_map | toYaml -}}
{{- end -}}
{{- end -}}

{{/*
 Get default URI for other service with release name
 */}}
{{- define "helm.release.other.default.uri" }}
{{- $alternate_context := (mergeOverwrite .context (dict "Release" (dict "Name" .release_name))) -}}
{{- $default_uri := include "temp.fill.templates" (dict "text" (.context.Values.default.uri) "context" $alternate_context) }}
{{- $default_uri -}}
{{- end -}}

