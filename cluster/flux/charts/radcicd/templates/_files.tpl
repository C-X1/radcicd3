{{/*
 Read a file from a path.
 Example:
 {{- $repo := include "files.fromPathReadFile" (dict "dir" $app_dir "file" "values.yaml" "context" $) | fromYaml }}
 */}}
{{- define "files.fromPathReadFile" -}}
{{- $filePath := printf "%s/%s" .dir .file -}}
{{- $fileContent := .context.Files.Get $filePath -}}
{{- $fileContent }}
{{- end -}}


{{/*
 Filter files by name. Return true if file should be filtered
 Example:
 {{- if not (include "file.filtered" (dict "file" $filepath "filter_files" $special_files)) }}
 */}}
{{- define "file.filtered" }}
{{- $filtered := false }}
{{- range $filter := $.filter_files }}
{{- if hasSuffix $filter $.file }}
{{- $filtered = true }}
{{- end }}
{{- end }}
{{- if $filtered -}}
{{- true -}}
{{- end }}
{{- end }}

{{- define "file.basename" }}
{{- last ( regexSplit "/" . -1 ) }}
{{- end }}

{{- define "file.notemplate" }}
{{- if hasPrefix "_" ( include "file.basename" . ) }}
{{- true -}}
{{- end }}
{{- end }}



{{/*
 Returns JSON list of all direct subdirectories under features/
 NOTE: To the directory being listed it must contain at least a single file in the first directory
 Example:

 {{ $myFeatures := include "file.list.feature.dirs" . | fromJsonArray }}

 */}}
{{- define "file.list.feature.dirs" -}}
{{- $feature_defaults := dict -}}
{{- range $path, $bytes := .Files.Glob "features/*/*" -}}
{{- $feature_dir := (base (dir $path)) -}}
{{- if not (hasKey $feature_defaults $feature_dir)  -}}
{{- $_ := set $feature_defaults $feature_dir (dict) -}}
{{- end -}}
{{- end -}}
{{- keys $feature_defaults | toJson -}}
{{- end -}}

{{/*
 Reads a file, applies context and afterwards transforms it from YAML
 {{- include "file.read.tpl.yaml" (dict "context" $context "files" $files "output" $output)  -}}
 */}}
{{- define "file.read.tpl.yaml" -}}


{{- range $filepath, $bytes := .files }}
{{ $docs := list }}

{{- $content := (tpl ($bytes | toString) $.context) }}

{{ range $part := regexSplit "\n---\n" $content -1 }}


{{- $part_yaml := $part | fromYaml }}
{{- $part_yaml_array := $part | fromYamlArray }}

{{- if not (hasPrefix "map[Error" ($part_yaml | toString)) }}
{{ $docs = append $docs $part_yaml }}
{{- end }}

{{- if not (hasPrefix "[error " ($part_yaml_array | toString)) }}
{{ $docs = append $docs $part_yaml_array }}
{{- end }}


{{- end }}
{{ $_ := set $.output $filepath $docs }}
{{- end }}
{{- end -}}
