"""Apply configurations to keycloak."""

import importlib
import json
import os
import pkgutil
import re
import sys
from typing import Dict, List

import yaml
from ansible.module_utils import basic
from ansible.module_utils.common.text.converters import to_bytes

USER_FEDERATION = "user_federation"


class KeycloakConfigApplicator(object):
    """Using Ansibles Keycloak modules to apply configuration."""

    def __init__(  # noqa: S107
        self,
        keycloak_url: str = "https://localhost:8080",
        admin_user: str = "admin",
        admin_password: str = "admin",
    ) -> None:
        """Init class.

        :param keycloak_url: The url of the keycloak instance
        :param admin_user: The username of the administrator account
        :param admin_password: The password of the administor account
        """
        self.bad_config_files = 0
        self.admin_user = admin_user
        self.admin_password = admin_password
        self.keycloak_url = keycloak_url

    @staticmethod
    def set_ansible_module_args(args: dict) -> None:
        """Set the module args for executing the current model.

        :param args: The dictionary with the args for the module
        """
        args["_ansible_keep_remote_files"] = False

        if "_ansible_remote_tmp" not in args:
            args["_ansible_remote_tmp"] = "/tmp"  # noqa: S108

        args = json.dumps({"ANSIBLE_MODULE_ARGS": args})
        basic._ANSIBLE_ARGS = to_bytes(args)  # noqa: WPS437

    def run_ansible_general_module(self, module: str, args: dict) -> None:
        """Run a keycloak module.

        :param module: Keycloak module name
        :param args: Keycloak module settings
        """
        KeycloakConfigApplicator.set_ansible_module_args(args)
        module = importlib.import_module(
            f"ansible_collections.community.general.plugins.modules.{module}"
        )
        module.main()

    def apply_config(self, module: str, config: dict) -> dict:
        """Apply configuration and return result."""

        return {}

    def run(self, config_directory: str) -> None:
        """Run configuration of keycloak.

        :param config_directory: The directory where to get the configuration files from
        """
        configuration = self.load_configurations(config_directory)

        for module in KeycloakConfigApplicator.list_keycloak_modules():

            result = self.apply_config(module, configuration[f"{module}s"])

            if module == USER_FEDERATION:
                pass

    def load_configurations(self, directory: str) -> Dict[str, list]:
        """Load all configurations from directory.

        :param directory: The directory to look for files (non recursively)
        :returns: Dictionary with configurations
        """
        configs = {}
        for keycloak_module in KeycloakConfigApplicator.list_keycloak_modules():
            config_name = f"{keycloak_module}s"

            configs[config_name] = self.merge_yaml_array_files_by_name_ending(
                config_name, directory
            )

        return configs

    @staticmethod
    def list_keycloak_modules() -> List[str]:  # noqa: WPS605
        """List all keycloak modules available in Ansibles community.general.

        :returns: List of keycloak modules available without "keycloak_" prefix
        """
        prefix = "keycloak_"

        general_modules = importlib.import_module(
            "ansible_collections.community.general.plugins.modules"
        )

        keycloak_modules = [
            module.name[len(prefix) :]
            for module in pkgutil.iter_modules(general_modules.__path__)
            if module.name.startswith(prefix)
        ]

        keycloak_modules.remove(USER_FEDERATION)
        keycloak_modules = [USER_FEDERATION] + keycloak_modules

        return keycloak_modules  # noqa:  WPS331

    @staticmethod
    def read_config_file(filepath: str) -> list:
        """Try to load the array from a yaml config file.

        :param filepath: The path of the file
        :returns: List of entries of the yaml file

        :raises RuntimeError: If matching file is not resulting in the list type
        :raises IOError: If filepath is not a file
        """
        if not os.path.isfile(filepath):
            raise IOError(f"{filepath} is not a file!")

        with open(filepath, "r") as config_file:
            filecontent = yaml.safe_load(config_file)
            if not isinstance(filecontent, list):
                raise RuntimeError(f"{filepath} is not an yaml array file!")

            return filecontent

    def merge_yaml_array_files_by_name_ending(  # noqa: WPS210
        self, ending: str, directory: str
    ) -> list:
        """Merge all yaml files ening with the same string.

        Note:
            The file name is case-insensitive.

        Example:
            namespace_default.release-name.<ending>.yaml
            namespace_default.release-name.<ending>.yml

        :param ending: The file ending before the extension
        :param directory: The directory to look for files (non recursively)
        :returns: The list of configurations of the specified file ending

        """
        pattern = f"(?i).*{ending}.(yml|yaml)"
        filenames = os.listdir(directory)
        config_collection = []

        for filename in filenames:
            if re.match(pattern, filename):
                filepath = f"{directory}/{filename}"

                configurations = []
                try:
                    configurations = KeycloakConfigApplicator.read_config_file(filepath)
                except (IOError, RuntimeError) as error:
                    sys.stderr.write(f"{error}\n")
                    sys.stderr.flush()
                    self.bad_config_files += 1

                config_collection += configurations

        return config_collection

