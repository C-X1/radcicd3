"""Test the config applicator."""

import yaml

from cluster.flux.charts.radcicd.keycloak.config_applicator import KeycloakConfigApplicator
from rpycihelpers.foldered_test import FolderedPyTest
from rpycihelpers.files import create_dir, create_file


class ConfigApplicatorTest(FolderedPyTest):
    """Test the KeycloakConfigApplicator."""

    @staticmethod
    def test_load_configurations() -> None:
        """Test load configurations function."""
        testobject = KeycloakConfigApplicator()
        realm0 = {'realm': 'master'}
        realm1 = {'realm': 'desaster'}
        user0 = {'name': 'master of desaster'}

        create_dir("test_configs")
        create_file("test_configs/realms0.realms.YaML", yaml.dump([realm0]))
        create_file("test_configs/realms1.realms.yml", yaml.dump([realm1]))
        create_file("test_configs/users0.users.yaml", yaml.dump([user0]))

        result = testobject.load_configurations("test_configs")

        assert isinstance(result, dict)
        assert "users" in result
        assert "realms" in result
        assert "user_federations" in result
        assert realm0 in result["realms"]
        assert realm1 in result["realms"]
        assert user0 in result["users"]
        assert isinstance(result["user_federations"], list)
        assert len(result["user_federations"]) == 0  # noqa: WPS507

    @staticmethod
    def test_list_keycloak_modules() -> None:
        """Test listing keycloak modules"""

        result = KeycloakConfigApplicator.list_keycloak_modules()

        assert isinstance(result, list)
        for name in (
            "role",
            "user_federation",
            "client_rolemapping",
            "group",
            "user"
        ):
            assert name in result, result

        assert result[0] == "user_federation"


    @staticmethod
    def test_merge_yaml_array_files_by_name_ending() -> None:
        """Test the collection of multiple files."""
        testobject = KeycloakConfigApplicator()
        realm0 = {'realm': 'master'}
        realm1 = {'realm': 'desaster'}
        user0 = {'name': 'master of desaster'}

        create_dir("configs")
        create_file("configs/realms0.realms.YaML", yaml.dump([realm0]))
        create_file("configs/realms1.realms.yml", yaml.dump([realm1]))
        create_file("configs/bad.realms.yml", "This is not a yaml file!")
        create_file("configs/users0.users.yaml", yaml.dump([user0]))

        result = testobject.merge_yaml_array_files_by_name_ending("realms", "configs")

        assert testobject.bad_config_files == 1
        assert isinstance(result, list)
        assert realm0 in result
        assert realm1 in result
        assert user0 not in result
